#!/usr/bin/env sh

GITPROJECT_DIR=$(dirname "$(echo "$0" | sed -e 's,\\,/,g')")
source $GITPROJECT_DIR/common.sh

usage()
{
	echo "usage: git project feature [list]"
	echo "       git project feature start <name>"
	echo "       git project feature finish <name>"
	echo "       git project feature continue <name>"
	echo "       git project feature fix <name>"
}

cmd_help()
{
	usage
	exit 0
}

cmd_default()
{
	cmd_list "$@"
}

cmd_list()
{
    git branch --list $(PREFIX_FEATURE)*
}

cmd_start()
{
    set -- $@
    NAME=$1
    develop_rebase

    echo "Creating a branch for the new feature: $NAME"
    git checkout -b $(PREFIX_FEATURE)$NAME

    rebase;
}

cmd_continue()
{
    set -- $@
    NAME=$1

    move_to_branch $(PREFIX_FEATURE)$NAME

    echo "All done! Now you can continue coding the feature: $NAME"
}

cmd_finish()
{
    set -- $@

    move_to_branch $(PREFIX_FEATURE)$1
    rebase

    echo "Pushing to "$(git config project.remote.fork)
	git push $(git config project.remote.fork) $(PREFIX_FEATURE)$1
	if [ $? -ne 0 ]; then
		echo "There was an error pusing to origin"
		exit 1
	fi

    echo "Creating Pull Request..."

	create_pull_request $(PREFIX_FEATURE)$1 $(git config project.branch.develop)
	if [ $? -ne 0 ]; then
		echo "There was an error creating the Pull Request. Please try manually"
		exit 1
	fi

	echo ""
	echo $(tput setaf 2)Pull Request created successfuly
	echo $(tput sgr0)""

	develop_rebase
}

cmd_fix()
{
    set -- $@
    NAME=$1

    move_to_branch $(PREFIX_FEATURE)$NAME

    echo "Pushing to "$(git config project.remote.fork)
    git push $(git config project.remote.fork) $(PREFIX_FEATURE)$NAME

    echo "All done! check your Pull Request to ensure the changes are present."
    echo "Note: In some hostings, you will need to create a new version of the Pull Request in order to add the new changes"

	develop_rebase
}
