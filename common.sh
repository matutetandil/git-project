#!/usr/bin/env sh

# https://confluence.atlassian.com/bitbucket/pullrequests-resource-423626332.html#pullrequestsResource-POST(create)anewpullrequest

readonly PREFIX_FEATURE=features/
readonly PREFIX_BUG=bugs/
readonly PREFIX_HOTFIX=hotfixes/
readonly PREFIX_RELEASE=releases/
readonly PREFIX_SNAPSHOT=snapshots/

readonly SERVICE_BITBUCKET=1
readonly SERVICE_GITHUB=2

configure_pull_requests()
{
    echo ""
    read -p "Do you want git-project to automatically create Pull Requests when you finish a feature?(y/n) " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        read -p "Pull Requests creation only works with either Bitbucket or GitHub, are you using any of those?(y/n) " -n 1 -r
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
            PS3='Please Select your service'
            options=("Bitbucket", "GitHub")
            select opt in "${options[@]}"
            do
                case $opt in
                    "Bitbucket")
                        echo ""
                        echo $(tput setaf 3)Please enter the Bitbucket Credentials. Will be used to create the Pull Requests.
                        echo $(tput setaf 3)Credentials are stored locally, so you don\'t need to worry about it.
                        echo $(tput setaf 3)You can always view/edit your credentials using:
                        echo ""
                        echo $(tput setaf 3)  git config project.bitbucket.username
                        echo $(tput setaf 3)  git config project.bitbucket.password
                        echo $(tput sgr0)""
                        read -p "Bitbucket username: " USERNAME
                        read -s -p "Bitbucket password: " PASSWORD

                        git config project.bitbucket.username $USERNAME
                        git config project.bitbucket.password $PASSWORD

                        git config.project.service $(SERVICE_BITBUCKET)

                        echo ""
                        echo $(tput setaf 3)Bitbucket configuration done!
                        break
                        ;;
                    "GitHub")
                        echo ""
                        if ! [ -x "$(command -v hub)" ]; then
                            echo $(tput setaf 1)WARNING: you need \'hub\' to use this feature.
                            echo $(tput setaf 1)I\'m gonna continue with the configuration, but you\'ll need to install hub manually.
                            echo $(tput setaf 1)hub URL: https://github.com/github/hub
                        fi
                        git config.project.service $(SERVICE_GITHUB)
                        echo ""
                        echo $(tput setaf 3)GitHub configuration done!
                        break
                        ;;
                    *) echo invalid option;;
                esac
            done
        fi
    fi
}

create_pull_request()
{
    if [ $(git config config.project.service) -eq $(SERVICE_BITBUCKET) ]; then
        USERNAME=$(git config project.username)
        PASSWORD=$(git config project.password)

        REPO=$(git remote -v | grep main | cut -d: -f2 | cut -d. -f1 | head -1)
        FORK=$(git remote -v | grep origin | cut -d: -f2 | cut -d. -f1 | head -1)

        TITLE=$(echo $1 | cut -d- -f2)

        OUTPUT=$(curl -s -X POST -H "Content-Type: application/json" -u $USERNAME:$PASSWORD https://api.bitbucket.org/2.0/repositories/$REPO/pullrequests -d '{"title": "'$TITLE'","source": {"branch": {"name": "'$1'"},"repository": {"full_name": "'$FORK'"}},"destination": {"branch": {"name": "'$2'"}},"close_source_branch": false}' | python -c "import json, sys; print json.load(sys.stdin)['id']")

        if [[ $OUTPUT =~ ^-?[0-9]+$ ]]; then
            return 0
        else
            return 1
        fi
    else
        TITLE=$(echo $1 | cut -d- -f2)
        hub pull-request -h "$TITLE" -F -
    fi
}

rebase_branch_with()
{
    git pull --rebase $(git config project.remote.main) $1
}

master_rebase()
{
    echo "Moving to master branch..."
    move_to_branch $(git config project.branch.master)

    echo "Rebasing from master"
    rebase_branch_with $(git config project.branch.master)
}

rebase()
{
    echo "Rebasing from Main..."
    rebase_branch_with $(git config project.branch.develop)
}

develop_rebase()
{
    echo "Moving to develop branch..."
    CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
    if [ $CURRENT_BRANCH != 'develop' ]; then
        git checkout develop
        if [ $? -ne 0 ]; then
            git checkout -b $(git config project.branch.develop) $(git config project.remote.main)/$(git config project.branch.develop)
        fi
    fi

    rebase;
}

move_to_branch()
{
    set -- $@

    CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
    if [ $CURRENT_BRANCH != "$1" ]; then
        echo "Moving to branch $1"
        git checkout $1
        if [ $? -ne 0 ]; then
            echo "The branch doesn't exist!"
            exit 1
        fi
    fi
}
