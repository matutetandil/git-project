# git-project

A collection of Git extensions to provide high-level repository operations for Vincent Driessen's branching model.

This script, unlike git-flow, is designed to work with the fork approach. Basically each developer, will have a fork from the original repo. All the changes will be pushed to the fork, and then the developer needs to create a Pull request to the main's develop branch.

This script, also adds several tools to easily solve common merge conflicts within rejected pull requests.

If you are familiar with nvie's git-flow script, you will notice that the commands and subcommands may be quite similar, but the internal functionality is very different. I made this, to make it easier to learn.

## Installing git-project
---
Installation is very easy:

1. Clone the git-project from *https://matutetandil@bitbucket.org/matutetandil/git-project.git*
2. Add the cloned directory to your path.
3. Verify if git-project is working: `git project version`

You will be see something like this:

    Git Project v0.1.0
    Developed by Matias Emanuel Denda Serrano <matutetandil@gmail.com>

## Starting from scratch
---
### Preconditions
* At least read access to the main repository
* Fork the main repository into your account
* If you want *git-project* to automatically create the Pull Requests, please be sure to have your Bitbucket credentials handy

### Get the code and start working
Create a new empty directory and go into it. Then run:

    git project init

The wizard will ask you a few questions about your main repository and your fork (also, if you want to, you will be prompted to enter Bitbucket credentials). After that, it will clone your fork, configure your remote, switch you to your develop branch and will make a rebase from your main repo.

Don't worry about the information you provide. It will be stored locally as git configuration's variables, so you can always view or change them.

The default configuration will be:

* Main repo name (*project.remote.main*): **main**
* Fork repo name (*project.remote.fork*): **origin**
* Master branch (*project.branch.master*): **master**
* Develop branch (*project.branch.develop*): **develop**
* Bitbucket username (*project.bitbucket.username*): **<your_username>**
* Bitbucket password (*project.bitbucket.password*): **<your_password>**

You can easily change this configuration running: `git project config`

## Setting up git-project in an existent project (or change the configuration)
---
In order to setup git-project in an existent project or to change git-project configuration, you need to run inside the project directory:

    git project config

The wizard will ask you for the main repo and fork repo names, master and develop branch names, and the Bitbucket credentials.

## Working with features
---
*git-project* makes it easy to work with features and saves your time. It will keep you working with the latest code in the main repository in every moment.

### Starting a new feature
To start a new feature, just run:

    git project feature start <your_feature>

### Finishing a feature
Once you complete the feature, and you did all the commits you want, run:

    git project feature finish <your_feature>

This command, makes a lot of things for you:

* Rebases you branch with your main's develop branch.
* Push your commits to your fork.
* Creates the Pull Request to the main's develop branch (if you configure your credentials).
* Switch to develop branch.
* Rebases your develop branch with main's develop branch.

### Continuing a feature
Sometimes, you may need to change from features to features. This is the purpose of this command. Basically, it doesn't do nothing new: just a branch switching, but prevents you do unnecessary rebases. To use it, jus run:

    git project feature continue <your_feature>

### Fixing a feature
Sometimes, you may need to do some modifications to an existing (and not merged) Pull Request. This command, in combination with continue, helps you to do that. Also prevents you make unnecessary (and dangerous) rebases. In order to successfully use this feature follow this simple steps:

1. If you aren't in the feature branch, run `git project feature continue <your_feature>` to change safely to the feature branch.
2. Make all the changes and commits you need.
3. Once you finish, run `git project feature fix <your_feature>`

This command, will push your commit(s) to the feature branch. Just that! no more and no less. But it'll ensure you to make it the right way!

## Working with Hotfixes
---
Working with hotfixes is analogue of working with features. You have the same functionality and subcommands.

The main difference with hotfixes, is that they start from the **master** branch, instead of the *develop* branch (as the features do). And when you finish a hotfix, git-project, will create two Pull Requests: one to *develop* branch and another to *master* branch.

The possible commands are:

* `git project hotfix start <your_hotfix>`
* `git project hotfix finish <your_hotfix>`
* `git project hotfix continue <your_hotfix>`
* `git project hotfix fix <your_hotfix>`

## Updating git project
In order to keep you up to date with the last changes, just go to the command line and type:

    git project update
